# Projet P4
Dans ce travail, je présente mon livrable pour l'UV Projet que j'ai suivi en P4, dans le cadre de ma formation à l'IMT Lille Douai.

J'ai travaillé sur le sujet intitulé : **Extraction de contours d'objets de petite taille à partir d'images couleurs** en étant encadrée par Madame Amel AISSAOUI

L'objectif de ce projet est de pouvoir transformer une image avec couleurs (image réelle) en une image sous forme de labels/contours.

Mon livrable est composé d'un dossier principal: 

** Segmentation**
Il contient la partie de la segmentation des images; la transformation des images à des labels différenciés selon les régions.


En plus, on trouve un rapport détaillé avec une présentation PPT pour la soutenance.




